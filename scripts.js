/* 
 * Exercise: Auto-Complete Control
 *
 * - On keyup, issue a GET to "data.json" (same origin) with the value of input field and the name of a callback function as parameters.
 * - There should be a minimum of 3 characters in the input field before issuing the GET.
 * - Upon successfully receiving data in JSON, update the results <ul>.
 * - If data is not received, print out the error statement “Data Not Available”.
 */

/*******************
 * Begin Code Here *
 *******************/



/*  loads json data:
	if data is successfully loaded, compare data to user input
	if data is not successfully loaded, send error message
*/
function loadJSON(input, callback) {
	var employeeInfo;

	$.ajax({
	    url:'data.json',
	    type:'GET',
	    dataType:'json',
	    success: function(res) {
	    	employeeInfo = res.data;
	    	if ($.isFunction(callback)){
	    		callback(input, employeeInfo);
	    	}
	    },
	    error: function( req, status, err ) {
	    	$('#results').append($("<li>Data Not Available</li>")).show();
		}
	});

	return employeeInfo;
}

function lookUp(input, data) {

	// set input to regex
	input = new RegExp(input.replace(/[^0-9a-z_]/i), 'i');

	// loop through json data and match input regex to either first name, last name, or employee id
	for (var i in data){
	    if (data[i].first.match(input) || data[i].last.match(input) || data[i].employeeId.match(input)) {
	        $('#results').append($("<li>" + data[i].first + 
	        						" " + data[i].last +
	        						" - " + data[i].employeeId + 
	        						"</li>"));
	    }
	}

	// show results
	$('#results').show();
}

$(function() {
	// on keyup after third char, issue a GET to "data.json"
	$('#employeeName').on({
		keyup: function(e) {

			// clear list
			$('#results').empty();

			// get user input
			var input = $(this).val();

			// if input is gt or equal to 3 chars, load json data
			if (input && input.length >= 3 && $.isFunction(loadJSON)){
				loadJSON(input, lookUp);
			} else {
				$('#results').hide();
			}
		}
	});
});
